package ru.stiznt.gfrv.githubfollowerrepview.LoginWindow;


import android.content.Intent;

import ru.stiznt.gfrv.githubfollowerrepview.MainWindow.MainActivity;

public class LoginPresenter {

    private LoginActivity mActivity;

    public LoginPresenter(LoginActivity activity){
        mActivity = activity;
    }

    public void loadFollowers(String login){
        if(login.isEmpty()){
            mActivity.emptyLogin();
            return;
        }
        Intent intent = new Intent(mActivity, MainActivity.class);
        intent.putExtra("login", login);
        mActivity.startActivity(intent);

    }
}
