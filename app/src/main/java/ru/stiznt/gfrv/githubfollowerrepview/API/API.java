package ru.stiznt.gfrv.githubfollowerrepview.API;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface API {
    @Headers("Accept:application/cnd.github.com.v3.full+json")

    @GET("/users/{login}")
    Call<Follower> getUser(@Path("login") String login);

    @GET("/users/{login}/followers")
    Call<List<Follower>> getFollowers(@Path("login") String login);

    @GET("/users/{login}/repos")
    Call<List<Rep>> getRepos(@Path("login") String login);
}
