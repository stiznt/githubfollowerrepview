package ru.stiznt.gfrv.githubfollowerrepview.LoginWindow;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ru.stiznt.gfrv.githubfollowerrepview.R;

public class LoginActivity extends AppCompatActivity {
    //var

    private Button mLoad;
    private TextInputEditText mLoginInput;
    private TextInputLayout mLoginInputLayout;
    private LoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //init
        mLoad = findViewById(R.id.load);
        mLoginInput = findViewById(R.id.login_input);
        mPresenter = new LoginPresenter(this);

        mLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.loadFollowers(mLoginInput.getText().toString());
            }
        });
    }

    public void emptyLogin(){
        mLoginInput.setError("Login is empty!");
    }
}
