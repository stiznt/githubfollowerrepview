package ru.stiznt.gfrv.githubfollowerrepview;

import android.app.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.stiznt.gfrv.githubfollowerrepview.API.API;

public class Main extends Application {

    private static API mApi;

    @Override
    public void onCreate() {
        super.onCreate();

        mApi = new Retrofit.Builder().baseUrl("https://api.github.com/").addConverterFactory(GsonConverterFactory.create()).build().create(API.class);

    }

    public static API getApi(){
        return mApi;
    }
}
