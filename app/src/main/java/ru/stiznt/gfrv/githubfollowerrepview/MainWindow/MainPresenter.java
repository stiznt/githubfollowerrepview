package ru.stiznt.gfrv.githubfollowerrepview.MainWindow;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.stiznt.gfrv.githubfollowerrepview.API.Follower;
import ru.stiznt.gfrv.githubfollowerrepview.API.Rep;
import ru.stiznt.gfrv.githubfollowerrepview.Main;

public class MainPresenter {

    private MainActivity mActivity;
    private List<Rep> mRepos;
    private List<Follower> mFollower;


    public MainPresenter(MainActivity activity){
        mActivity = activity;
        mRepos = new ArrayList<>();
        mFollower = new ArrayList<>();



    }

    public List<Rep> getRepos(String login){
        return mRepos;
    }

    public List<Follower> getFollowers(String login){
        return mFollower;
    }
}
