package ru.stiznt.gfrv.githubfollowerrepview.MainWindow;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.stiznt.gfrv.githubfollowerrepview.API.Follower;
import ru.stiznt.gfrv.githubfollowerrepview.R;
import ru.stiznt.gfrv.githubfollowerrepview.API.Rep;

public class MainActivity extends AppCompatActivity {

    //var
    private MainPresenter mPresenter;
    private TextView mLogin, mInfo, mListName;
    private RecyclerView mList;
    private ImageView mAvatar;
    private MainListAdapter adapter;

    private boolean isFollowers = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //action bar create
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        //init
        mPresenter = new MainPresenter(this);
        mLogin = findViewById(R.id.login);
        mInfo = findViewById(R.id.info);
        mListName = findViewById(R.id.list_name);
        mList = findViewById(R.id.list);
        mAvatar = findViewById(R.id.avatar);
        adapter = new MainListAdapter(this);
        String login = getIntent().getExtras().getString("login");
        setInfo(login);


        mList.setAdapter(adapter);
        mList.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setInfo(String login){
        List<Rep> repos =  mPresenter.getRepos(login);
        List<Follower> followers = mPresenter.getFollowers(login);
        if(isFollowers){
            mLogin.setText(login);
            mInfo.setText("Repositories: " + repos.size() + "\nFollowers: " + followers.size());
            mListName.setText("Followers");
            adapter.setItems(followers);
        }else{
            mLogin.setText(login);
            mInfo.setText("Repositories: " + repos.size());
            mListName.setText("Repositories");
            adapter.setItems(repos);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            if(isFollowers)this.finish();
            isFollowers = true;
            setInfo(getIntent().getExtras().getString("login"));
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if(isFollowers) this.finish();
        isFollowers = true;
        setInfo(getIntent().getExtras().getString("login"));
    }

    public void toRepos(){
        isFollowers = false;
    }

    public void toFollowers(){
        isFollowers = true;
    }

    public void setError(){

    }
}
