package ru.stiznt.gfrv.githubfollowerrepview.MainWindow;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.stiznt.gfrv.githubfollowerrepview.API.Follower;
import ru.stiznt.gfrv.githubfollowerrepview.R;
import ru.stiznt.gfrv.githubfollowerrepview.API.Rep;

public class MainListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> objs;
    private MainActivity mActivity;

    public MainListAdapter(MainActivity activity){
        objs = new ArrayList<>();
        mActivity = activity;
    }

    public <T> void setItems(List<T> list){
        objs.clear();
        objs.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if(objs.get(position) instanceof Follower){
            return 1;
        }else if(objs.get(position) instanceof Rep){
            return 2;
        }
        return -1;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case 1:
                ((FollowerMainListItem)holder).bind(objs.get(position));
                break;
            case 2:
                ((RepMainListItem) holder).bind(objs.get(position));
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = null;
        if(getItemViewType(i) == 1){
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.follower_mainlist_item, viewGroup, false);
            return new FollowerMainListItem(v);
        }else if(getItemViewType(i) == 2){
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rep_mainlist_item, viewGroup, false);
            return new RepMainListItem(v);
        }
        return null;
    }



    @Override
    public int getItemCount() {
        return objs.size();
    }

    class FollowerMainListItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mAvatar;
        private TextView mLogin;
        private CardView mCard;

        public FollowerMainListItem(View v){
            super(v);
            mAvatar = v.findViewById(R.id.avatar);
            mLogin = v.findViewById(R.id.login);
            mCard = v.findViewById(R.id.card);

            mCard.setOnClickListener(this);
        }

        public void bind(Object obj){
            Follower f = (Follower) obj;
            mLogin.setText("same login");
        }

        @Override
        public void onClick(View v) {
            mActivity.toRepos();
            mActivity.setInfo(mLogin.getText().toString());
        }
    }

    class RepMainListItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mName, mDescription, mStars;
        private CardView mCard;
        private Uri url;

        public RepMainListItem(View v){
            super(v);

            mName = v.findViewById(R.id.name);
            mDescription = v.findViewById(R.id.description);
            mStars = v.findViewById(R.id.stars);
            mCard = v.findViewById(R.id.card);
            url = null;
            mCard.setOnClickListener(this);
        }

        public void bind(Object obj){
            Rep r = (Rep) obj;
            mName.setText("kek");
            mDescription.setText("Desc kek azazazazaza");
            mStars.setText(10 + "");
            url = Uri.parse("https://yandex.ru");
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Intent.ACTION_VIEW, url);
            mActivity.startActivity(intent);
        }
    }
}
